import { Sqlite } from '@/deps.ts';

import { DB_PATH } from './config.ts';
import CipherDB from './cipher-db.ts';
import FollowsDB from './follows-db.ts';

const db = new Sqlite(DB_PATH);
const cipher = new CipherDB(db);
const followsDB = new FollowsDB(db);

export { cipher, db, followsDB };
