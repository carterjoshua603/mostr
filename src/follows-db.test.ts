import { assert, assertEquals, Sqlite } from './deps.ts';
import FollowsDB from './follows-db.ts';

const sqlite = new Sqlite();

const alex = 'https://gleasonator.com/users/alex';
const jack = '82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2';

Deno.test('FollowsDB', () => {
  const db = new FollowsDB(sqlite);
  assertEquals(db.getFollowers(jack), []);
  assert(!db.hasFollowers(jack));

  db.addFollow(alex, jack);
  assertEquals(db.getFollowers(jack), [alex]);
  assert(db.hasFollowers(jack));

  db.addFollow(alex, jack);
  db.removeFollow(alex, jack);
  assertEquals(db.getFollowers(jack), []);
  assert(!db.hasFollowers(jack));
});
