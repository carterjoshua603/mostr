import { getSecretKey } from '@/config.ts';
import { getEventHash, getSignature, secp } from '@/deps.ts';

import type { Event, EventTemplate, SignedEvent } from './event.ts';

/**
 * Get SHA-256 digest bytes from a string.
 * <https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest#basic_example>
 */
async function getDigest(message: string) {
  const encoder = new TextEncoder();
  const data = encoder.encode(message);
  const hash = await crypto.subtle.digest('SHA-256', data);
  return hash;
}

/** Generate Nostr keys from a seed. */
async function generateKeys(seed: string) {
  const privateKeyBuff = await getDigest(seed);
  const privateKey = secp.utils.bytesToHex(new Uint8Array(privateKeyBuff));

  return {
    privateKey,
    publicKey: secp.utils.bytesToHex(secp.schnorr.getPublicKey(privateKey)),
  };
}

/** Get Nostr keys for an ActivityPub ID. */
function getActorKeys(apId: string) {
  return generateKeys(getSecretKey() + ':' + apId);
}

/*
 * STOP! Only the functions below should be exported.
 *
 * For security reasons, it's important we only expose a NIP-07-like API
 * to the rest of the codebase. Other modules should not be able to access
 * private keys directly, just perform the actions exported by this file.
 */

/** Get Nostr pubkey for an ActivityPub ID. */
async function getPublicKey(apId: string): Promise<string> {
  const { publicKey } = await getActorKeys(apId);
  return publicKey;
}

/** Sign Nostr event by the ActivityPub user. */
async function signEvent<K extends number = number>(event: EventTemplate<K>, apId: string): Promise<SignedEvent<K>> {
  const { publicKey, privateKey } = await getActorKeys(apId);

  (event as Event<K>).pubkey = publicKey;
  (event as Event<K>).id = getEventHash(event as Event<K>);
  (event as Event<K>).sig = getSignature(event as Event<K>, privateKey);

  return event as SignedEvent<K>;
}

export { getPublicKey, signEvent };
