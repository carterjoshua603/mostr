import { Event } from './event.ts';

const isEventTag = (tag: string[]) => tag[0] === 'e';
const isReplyTag = (tag: string[]) => tag[0] === 'e' && tag[3] === 'reply';
const isQuoteTag = (tag: string[]) => tag[0] === 'e' && tag[3] === 'mention';
const isRootTag = (tag: string[]) => tag[0] === 'e' && tag[3] === 'root';

const isPubkeyTag = (tag: string[]) => tag[0] === 'p';
const isLegacyReplyTag = (tag: string[]) => tag[0] === 'e' && !tag[3];

function findReplyTag({ tags }: Event<1>) {
  return tags.find(isReplyTag) || tags.find(isRootTag) || tags.findLast(isLegacyReplyTag);
}

const isHashtag = (tag: string[]) => tag[0] === 't';
const isEmojiTag = (tag: string[]) => tag[0] === 'emoji';

const isCWTag = (tag: string[]) => tag[0] === 'content-warning';
const hasCWTag = ({ tags }: Event) => tags.some(isCWTag);

const isMostrTag = (tag: string[]) => tag[0] === 'mostr';
const hasMostrTag = ({ tags }: Event) => tags.some(isMostrTag);

export {
  findReplyTag,
  hasCWTag,
  hasMostrTag,
  isCWTag,
  isEmojiTag,
  isEventTag,
  isHashtag,
  isMostrTag,
  isPubkeyTag,
  isQuoteTag,
  isReplyTag,
  isRootTag,
};
