import { DOMParser, type Element, type HTMLDocument, nip19 } from '@/deps.ts';
import { toPubkey } from '../utils.ts';

/** Get DOM object from HTML string. */
function parseContent(html: string): HTMLDocument | null {
  return new DOMParser().parseFromString(html, 'text/html');
}

/** Remove fallback markup for supported features. This mutates the doc. */
function stripCompatFeatures(doc: HTMLDocument): void {
  const selectors = [
    '.quote-inline', // Quote posting
    '.recipients-inline', // Explicit mentions
  ];

  selectors.forEach((selector) => {
    doc.querySelectorAll(selector).forEach((elem) => {
      (elem as Element).remove();
    });
  });
}

/** Rewrite inline mentions to NIP-21 URIs, and returns the pubkeys. Mutates the doc. */
function processMentions(doc: HTMLDocument): Promise<string[]> {
  const nodes = doc.querySelectorAll('a.mention.u-url');

  return [...nodes].reduce<Promise<string[]>>(async (result, node) => {
    const mention = node as Element;
    const apId = mention.getAttribute('href');
    const pubkey = apId ? await toPubkey(apId) : undefined;

    if (pubkey) {
      mention.innerHTML = `nostr:${nip19.npubEncode(pubkey)}`;
      return [...await result, apId!];
    }

    return result;
  }, Promise.resolve([]));
}

export { parseContent, processMentions, stripCompatFeatures };
