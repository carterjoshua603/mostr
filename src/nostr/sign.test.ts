import { assert, assertEquals, verifySignature } from '@/deps.ts';

import { getPublicKey, signEvent } from './sign.ts';

const ALEX_AP_ID = 'https://gleasonator.com/users/alex';
const ALEX_PUBKEY = '851cf515e4fe1301432ab2d966acdbc4a7c16942eb2fa909d823f62dd9c9f21b';

Deno.test('getPublicKey', async () => {
  const pubkey = await getPublicKey(ALEX_AP_ID);
  assertEquals(pubkey, ALEX_PUBKEY);
});

Deno.test('getPublicKey is determined by SECRET_KEY', async () => {
  const SECRET_KEY = Deno.env.get('SECRET_KEY')!;

  Deno.env.set('SECRET_KEY', '1');
  const pubkey1 = await getPublicKey(ALEX_AP_ID);
  assertEquals(pubkey1, 'fb8ef9f5ec3e4eeb0987ab2907d5480c341345401468a3d64ef06a539971b1ba');

  Deno.env.set('SECRET_KEY', '2');
  const pubkey2 = await getPublicKey(ALEX_AP_ID);
  assertEquals(pubkey2, '42a1ad34804503ef847c6ce3ddd51cc2af99b4dbfaeed49833eb5e9c04ae5d29');

  // Sanity check: these keys are not the same!
  assert(pubkey1 !== pubkey2);

  // Reset back to original
  Deno.env.set('SECRET_KEY', SECRET_KEY);
});

Deno.test('signEvent', async () => {
  const template = {
    kind: 1,
    content: 'hello world',
    tags: [],
    created_at: 0,
  };

  const event = await signEvent(template, ALEX_AP_ID);

  assertEquals(event.pubkey, ALEX_PUBKEY);
  assertEquals(event.id, '9cd585780b5240e08fa9e5c28cda00c6f9ec90ae97958ad200cdfbafbbb9467b');
  assert(verifySignature(event));
});
