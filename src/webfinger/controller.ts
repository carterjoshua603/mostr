import { LOCAL_DOMAIN } from '../config.ts';
import { fetchUser } from '../nostr/client.ts';

import { toWebfinger } from './transmute.ts';

import type { Context } from '../deps.ts';

async function webfingerController(c: Context) {
  try {
    const { hostname } = new URL(LOCAL_DOMAIN);
    const resource = c.req.query('resource');
    const acct = resource.split('acct:')[1];
    const [pubkey, host] = acct.split('@');

    if (host !== hostname) {
      throw hostname;
    }

    const event = await fetchUser(pubkey);

    if (event) {
      const actor = toWebfinger(event);
      c.header('content-type', 'application/jrd+json');
      return c.body(JSON.stringify(actor));
    } else {
      return c.json({ error: 'Not found' }, 404);
    }
  } catch (_e) {
    return c.json({ error: 'Bad request' }, 400);
  }
}

function hostMetaController(c: Context) {
  const template = new URL('/.well-known/webfinger?resource={uri}', LOCAL_DOMAIN).toString();

  c.header('content-type', 'application/xrd+xml');
  return c.body(
    `<?xml version="1.0" encoding="UTF-8"?><XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0"><Link rel="lrdd" template="${template}" type="application/xrd+xml" /></XRD>`,
  );
}

export { hostMetaController, webfingerController };
