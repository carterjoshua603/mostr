import { LOCAL_DOMAIN } from '../config.ts';
import { url } from '../utils/parse.ts';

import type { Event } from '../nostr/event.ts';
import type { Webfinger } from './webfingerSchema.ts';

/** Present Nostr user on Webfinger. */
const toWebfinger = (event: Event<0>): Webfinger => {
  const { host } = new URL(LOCAL_DOMAIN);
  const apId = url(`/users/${event.pubkey}`);

  return {
    subject: `acct:${event.pubkey}@${host}`,
    aliases: [apId],
    links: [
      {
        rel: 'self',
        type: 'application/activity+json',
        href: apId,
      },
      {
        rel: 'self',
        type: 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"',
        href: apId,
      },
    ],
  };
};

export { toWebfinger };
