import { assert, assertEquals, pemToPublicKey } from '../deps.ts';

import { getPrivateKey, getPublicKeyPem } from './keys.ts';

Deno.test('getPublicKeyPem', async () => {
  const userId = 'benis';

  const publicPem = await getPublicKeyPem(userId);
  const publicKey = await pemToPublicKey(publicPem);
  const privateKey = await getPrivateKey(userId);

  const message = new TextEncoder().encode('hello world!');
  const algorithm = 'RSASSA-PKCS1-v1_5';

  const signature = await window.crypto.subtle.sign(
    algorithm,
    privateKey,
    message,
  );

  const valid = await crypto.subtle.verify(
    algorithm,
    publicKey,
    signature,
    message,
  );

  assert(valid);
});

Deno.test('getPublicKeyPem is determined by SECRET_KEY', async () => {
  const SECRET_KEY = Deno.env.get('SECRET_KEY')!;

  Deno.env.set('SECRET_KEY', '1');
  const publicPem1 = await getPublicKeyPem('https://fedibird.com/users/alex');
  assertEquals(
    publicPem1,
    '-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCOmZ/MzuCQ8fn0VYySfItDpf2Y24NfXgTjq+s0BXbCeojEhYyHBlOWh68KqK4KY+aEP45563D/2NTWvFQgepvFtdr1B/z/2EHr3Ypduup0LZ5+3wM3tNxEem45xfCoRTU9j4HP1kZ/gbZs9fYBJrjZoYJ9HEBYG77ftMTglG2GpQIDAQAB\n-----END PUBLIC KEY-----',
  );

  Deno.env.set('SECRET_KEY', '2');
  const publicPem2 = await getPublicKeyPem('https://fedibird.com/users/alex');
  assertEquals(
    publicPem2,
    '-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCak0akxASH4zQ94K/e/V42i334tG17cEZ6PwAl/Wx+9fl2I/VBuGD3zKTo2c3kO8F7SiA6+sitDGusoXJ+8qUZBE1yspuLWjFBNbi7+bGxLpiOmVCYRzK3LIfvk13TaMnYD6Cc4KXw4sKPAsRWgNvH8N4pYKIEyIRGU7y8A1MgjwIDAQAB\n-----END PUBLIC KEY-----',
  );

  // Sanity check: these keys are not the same!
  assert(publicPem1 !== publicPem2);

  // Reset back to original
  Deno.env.set('SECRET_KEY', SECRET_KEY);
});
