import { getSecretKey } from '../config.ts';
import { generateSeededRsa, ParsedSignature, pemToPublicKey, publicKeyToPem } from '../deps.ts';

import { fetchObject } from './client.ts';
import { isActor } from './utils.ts';

const opts = {
  bits: 1024,
};

const buildSeed = (userId: string) => getSecretKey() + ':' + userId;

async function getPublicKeyPem(userId: string): Promise<string> {
  const seed = buildSeed(userId);
  const { publicKey } = await generateSeededRsa(seed, opts);
  return publicKeyToPem(publicKey);
}

async function getPrivateKey(userId: string): Promise<CryptoKey> {
  const seed = buildSeed(userId);
  const { privateKey } = await generateSeededRsa(seed, opts);
  return privateKey;
}

/** Get actor public key from HTTP Signature for verification. */
async function pubKeyGetter(sig: ParsedSignature) {
  const url = new URL(sig.keyId);
  url.hash = '';

  const actor = await fetchObject(url.toString());

  if (actor && isActor(actor) && actor.publicKey) {
    return pemToPublicKey(actor.publicKey.publicKeyPem);
  } else {
    return null;
  }
}

export { getPrivateKey, getPublicKeyPem, pubKeyGetter };
