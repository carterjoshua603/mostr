import { LOCAL_DOMAIN } from '../../config.ts';
import { verifyRequest } from '../../deps.ts';
import handleActivity from '../handler.ts';
import { pubKeyGetter } from '../keys.ts';
import { Activity, activitySchema } from '../schema.ts';

import type { Context } from '../../deps.ts';

async function inboxController(c: Context<'username?'>) {
  let activity: Activity;
  let verified: boolean;

  try {
    activity = activitySchema.parse(await c.req.json());
  } catch (_e) {
    return new Response('Bad activity', { status: 400 });
  }

  if (activity.id.startsWith(LOCAL_DOMAIN) || activity.actor.startsWith(LOCAL_DOMAIN)) {
    return new Response('NO STOP YOU CAN\'T DO THAT', { status: 400 });
  }

  try {
    verified = await verifyRequest(c.req as Request, async (sig) => (
      // Prevent spoofing.
      sig.keyId.startsWith(activity.actor) ? await pubKeyGetter(sig) : null
    ));
  } catch (_e) {
    verified = false;
  }

  if (!verified) {
    return new Response('Invalid HTTP signature', { status: 400 });
  }

  handleActivity(activity);

  return new Response('', { status: 200 });
}

export { inboxController };
