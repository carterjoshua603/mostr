import { linkify, linkifyStr, mime, nip19, nip21, nip27, validateEvent, verifySignature } from '@/deps.ts';

import { AP_PUBLIC_URI } from '../activitypub/constants.ts';
import { getPublicKeyPem } from '../activitypub/keys.ts';
import { LOCAL_DOMAIN } from '../config.ts';
import { cipher } from '../db.ts';
import { MetaContent, metaContentSchema } from '../nostr/schema.ts';
import { findReplyTag, isCWTag, isEmojiTag, isEventTag, isHashtag, isPubkeyTag, isQuoteTag } from '../nostr/tags.ts';
import { date, url } from '../utils/parse.ts';

import { fetchObject } from './client.ts';
import { getActorAcct, isActor } from './utils.ts';

import type { Event } from '../nostr/event.ts';
import type { Actor, Announce, Delete, Emoji, EmojiReact, Hashtag, Like, Mention, Note, Proxy, Zap } from './schema.ts';

linkify.registerCustomProtocol('nostr', true);

const getActorId = (pubkey: string) => cipher.getApId(pubkey) || url(`/users/${pubkey}`);
const getObjectId = (eventId: string) => cipher.getApId(eventId) || url(`/objects/${eventId}`);

/** Get pubkey from decoded bech32 entity, or undefined if not applicable. */
function getDecodedPubkey(decoded: nip19.DecodeResult): string | undefined {
  switch (decoded.type) {
    case 'npub':
      return decoded.data;
    case 'nprofile':
      return decoded.data.pubkey;
  }
}

/** This is a hack to get the username from a URL, which works most of the time. */
function inferUsernameFromApId(apId: string): string | undefined {
  const match = apId.match(/(?:\/users\/|\/@)([^/]+)$/);
  if (match) {
    return match[1];
  }
}

const linkifyOpts: linkify.Opts = {
  render: {
    hashtag: ({ content }) => {
      const tag = content.replace(/^#/, '');
      const href = url(`/tags/${tag}`);
      return `<a class=\"mention hashtag\" href=\"${href}\" rel=\"tag\"><span>#</span>${tag}</a>`;
    },
    url: ({ content }) => {
      try {
        const { decoded } = nip21.parse(content);
        const pubkey = getDecodedPubkey(decoded);
        if (pubkey) {
          const pubk = pubkey.substring(0, 8);
          const apId = cipher.getApId(pubkey);
          const href = apId ? apId : url(`/users/${pubkey}`);
          const name = apId ? inferUsernameFromApId(apId) || pubk : pubk;
          return `<span class="h-card"><a class="u-url mention" href="${href}" rel="ugc">@<span>${name}</span></a></span>`;
        } else {
          return '';
        }
      } catch (_e) {
        return `<a href="${content}">${content}</a>`;
      }
    },
  },
};

/** Nostr metadata event to ActivityPub actor. */
const toActor = async (event: Event<0>): Promise<Actor> => {
  const parsed = metaContentSchema.safeParse(JSON.parse(event.content));
  const content: MetaContent = parsed.success ? parsed.data : {};

  const publicKey = await getPublicKeyPem(url(`/users/${event.pubkey}`));

  const emojis = event.tags
    .filter(isEmojiTag)
    .map(toEmoji);

  return {
    type: 'Person',
    id: url(`/users/${event.pubkey}`),
    name: content?.name || '',
    preferredUsername: event.pubkey,
    inbox: url(`/users/${event.pubkey}/inbox`),
    followers: url(`/users/${event.pubkey}/followers`),
    following: url(`/users/${event.pubkey}/following`),
    outbox: url(`/users/${event.pubkey}/outbox`),
    icon: content.picture
      ? {
        type: 'Image',
        url: content.picture,
      }
      : undefined,
    image: content.banner
      ? {
        type: 'Image',
        url: content.banner,
      }
      : undefined,
    summary: linkifyStr(content.about || '', linkifyOpts),
    attachment: [],
    tag: emojis,
    publicKey: {
      id: url(`/users/${event.pubkey}#main-key`),
      owner: url(`/users/${event.pubkey}`),
      publicKeyPem: publicKey,
    },
    endpoints: {
      sharedInbox: url('/inbox'),
    },
    proxyOf: [toNpubProxy(event)],
  };
};

function toEmoji(tag: string[]): Emoji {
  return {
    type: 'Emoji',
    icon: {
      type: 'Image',
      url: tag[2],
    },
    name: `:${tag[1]}:`,
  };
}

async function toMention(tag: string[]): Promise<Mention | undefined> {
  if (!isPubkeyTag(tag)) return;
  const apId = cipher.getApId(tag[1]);

  if (apId) {
    const actor = await fetchObject(apId);
    if (actor && isActor(actor)) {
      return {
        type: 'Mention',
        href: apId,
        name: `@${getActorAcct(actor)}`,
      };
    }
  }

  const { host } = new URL(LOCAL_DOMAIN);
  return {
    type: 'Mention',
    href: url(`/users/${tag[1]}`),
    name: `@${tag[1]}@${host}`,
  };
}

const parseMentions = (content: string, mentions: (Mention | undefined)[]): string => {
  return mentions.reduce((content, mention, i) => {
    if (!mention) return content;
    const [_, user] = mention.name!.split('@');
    return content.replaceAll(
      `#[${i}]`,
      `<span class="h-card"><a class="u-url mention" href="${mention.href}" rel="ugc">@<span>${
        user.substring(0, 8)
      }</span></a></span>`,
    );
  }, content);
};

function isMediaLink(link: string): boolean {
  const { pathname } = new URL(link);
  const mimeType = mime.getType(pathname);
  if (!mimeType) return false;

  const [baseType, _subType] = mimeType.split('/');
  return ['audio', 'image', 'video'].includes(baseType);
}

/** Remove any lingering tokens #[0] and replace linebreaks with <br> tags. */
function cleanContent(content: string) {
  return content
    .replace(/\n{0,2}#\[\d\]$/, '')
    .replace(/\s+$/, '')
    .replaceAll('\n', '<br />');
}

/** Nostr text event to ActivityPub Note. */
const toNote = async (event: Event<1>): Promise<Note> => {
  const imentions = await Promise.all(event.tags.map(toMention));
  const mentions = imentions.filter((m): m is Mention => Boolean(m));
  const linkified = linkifyStr(event.content, linkifyOpts);
  const cwTag = event.tags.find(isCWTag);

  const mediaLinks = linkify.find(event.content, 'url')
    .map((link) => link.href)
    .filter(isMediaLink);

  const hashtags: Hashtag[] = event.tags.filter(isHashtag).map((tag) => ({
    type: 'Hashtag',
    href: url(`/tags/${tag[1]}`),
    name: `#${tag[1]}`,
  }));

  const emojis = event.tags
    .filter(isEmojiTag)
    .map(toEmoji);

  return {
    type: 'Note',
    id: url(`/objects/${event.id}`),
    attributedTo: url(`/users/${event.pubkey}`),
    content: cleanContent(parseMentions(linkified, imentions)),
    to: [
      AP_PUBLIC_URI,
      ...mentions.map((m) => m.href),
    ],
    cc: [url(`/users/${event.pubkey}/followers`)],
    tag: [...mentions, ...hashtags, ...emojis],
    attachment: mediaLinks.map((link) => ({
      type: 'Document',
      url: link,
      mediaType: mime.getType(link) || undefined,
    })),
    sensitive: Boolean(cwTag),
    summary: cwTag ? cwTag[1] : undefined,
    published: date(event.created_at),
    inReplyTo: getInReplyTo(event),
    quoteUrl: getQuoteUrl(event),
    proxyOf: [toNoteProxy(event)],
  };
};

function getInReplyTo(event: Event<1>): string | undefined {
  const replyTag = findReplyTag(event);

  if (replyTag) {
    return getObjectId(replyTag[1]);
  }
}

function findQuoteInContent(event: Event): string | undefined {
  try {
    for (const { decoded } of nip27.matchAll(event.content)) {
      switch (decoded.type) {
        case 'note':
          return decoded.data;
        case 'nevent':
          return decoded.data.id;
      }
    }
  } catch (_) {
    // do nothing
  }
}

function findLegacyQuoteTag(event: Event): string[] | undefined {
  return event.tags.find((tag, i) => {
    return isEventTag(tag) && event.content.includes(`#[${i}]`);
  });
}

function findQuoteInTags(event: Event): string | undefined {
  const quoteTag = event.tags.find(isQuoteTag) || findLegacyQuoteTag(event);

  if (quoteTag) {
    return quoteTag[1];
  }
}

function getQuoteUrl(event: Event): string | undefined {
  const quoteId = findQuoteInContent(event) || findQuoteInTags(event);

  if (quoteId) {
    return getObjectId(quoteId);
  }
}

function toAnnounce(event: Event<1> | Event<6>): Announce | undefined {
  const objectId = getQuoteUrl(event);
  if (!objectId) return;

  return {
    type: 'Announce',
    id: url(`/objects/${event.id}`),
    object: objectId,
    actor: url(`/users/${event.pubkey}`),
    to: [AP_PUBLIC_URI],
    cc: [url(`/users/${event.pubkey}/followers`)],
    published: date(event.created_at),
    proxyOf: [toNoteProxy(event)],
  };
}

function getReactedEvent(event: Event): string | undefined {
  const eventTag = event.tags.findLast(isEventTag);

  if (eventTag) {
    return getObjectId(eventTag[1]);
  }
}

function getTaggedUsers(event: Event): string[] {
  return event.tags
    .filter(isPubkeyTag)
    .map((tag) => getActorId(tag[1]));
}

function toLike(event: Event<7>): Like | undefined {
  const reactedEvent = getReactedEvent(event);
  if (!reactedEvent) return;
  const mentions = getTaggedUsers(event);

  return {
    type: 'Like',
    id: url(`/objects/${event.id}`),
    object: reactedEvent,
    actor: url(`/users/${event.pubkey}`),
    to: [AP_PUBLIC_URI, ...mentions],
    cc: [url(`/users/${event.pubkey}/followers`)],
    proxyOf: [toNoteProxy(event)],
  };
}

function toEmojiReact(event: Event<7>): EmojiReact | undefined {
  const reactedEvent = getReactedEvent(event);
  if (!reactedEvent) return;
  const mentions = getTaggedUsers(event);

  return {
    type: 'EmojiReact',
    id: url(`/objects/${event.id}`),
    object: reactedEvent,
    content: event.content,
    actor: url(`/users/${event.pubkey}`),
    to: [AP_PUBLIC_URI, ...mentions],
    cc: [url(`/users/${event.pubkey}/followers`)],
    proxyOf: [toNoteProxy(event)],
  };
}

function toDelete(event: Event<5>): Delete | undefined {
  const reactedEvent = getReactedEvent(event);
  if (!reactedEvent) return;

  return {
    type: 'Delete',
    id: url(`/objects/${event.id}`),
    object: reactedEvent,
    actor: url(`/users/${event.pubkey}`),
    to: [AP_PUBLIC_URI],
    cc: [url(`/users/${event.pubkey}/followers`)],
    proxyOf: [toNoteProxy(event)],
  };
}

function getZappedActor(event: Event<9734 | 9735>) {
  const tag = event.tags.find(isPubkeyTag);

  if (tag) {
    return getActorId(tag[1]);
  }
}

/** The zapped object might be a Note _or_ a Person. */
function getZappedObject(event: Event<9734 | 9735>) {
  const tag = event.tags.find(isEventTag) || event.tags.find(isPubkeyTag);

  if (tag && tag[1]) {
    switch (tag[0]) {
      case 'e':
        return getObjectId(tag[1]);
      case 'p':
        return getActorId(tag[1]);
    }
  }
}

/** Parse the inner zap request event from the zap. */
function getZapRequest(event: Event<9735>): Event<9734> | undefined {
  const description = event.tags.find((tag) => tag[0] === 'description');

  try {
    const zapRequest = JSON.parse(description![1]);
    if (validateEvent(zapRequest) && zapRequest.kind === 9734 && verifySignature(zapRequest)) {
      return zapRequest as Event<9734>;
    }
  } catch (_e) {
    // do nothing
  }
}

function toZap(event: Event<9735>): Zap | undefined {
  const zapRequest = getZapRequest(event);
  if (!zapRequest) return;
  const zappedActor = getZappedActor(event) || getZappedActor(zapRequest);
  if (!zappedActor) return;
  const zappedObject = getZappedObject(event) || getZappedObject(zapRequest);
  if (!zappedObject) return;

  return {
    type: 'Zap',
    id: url(`/objects/${event.id}`),
    object: zappedObject,
    actor: url(`/users/${zapRequest.pubkey}`),
    to: [AP_PUBLIC_URI, zappedActor],
    cc: [url(`/users/${zapRequest.pubkey}/followers`)],
    proxyOf: [toNoteProxy(event)],
  };
}

const NOSTR_PROTOCOL_URI = 'https://github.com/nostr-protocol/nostr';

function toNpubProxy(event: Event): Proxy {
  return {
    protocol: NOSTR_PROTOCOL_URI,
    proxied: nip19.npubEncode(event.pubkey),
    authoritative: true,
  };
}

function toNoteProxy(event: Event): Proxy {
  return {
    protocol: NOSTR_PROTOCOL_URI,
    proxied: nip19.noteEncode(event.id!),
    authoritative: true,
  };
}

export { toActor, toAnnounce, toDelete, toEmojiReact, toLike, toNote, toZap };
