import {
  actorController,
  followersController,
  followingController,
  inboxController,
  objectController,
} from './activitypub/controllers/mod.ts';
import { outboxController } from './activitypub/controllers/outbox.ts';
import { cors, Hono } from './deps.ts';
import { indexController } from './indexController.ts';
import { cache } from './middleware/cache.ts';
import { nip19Redirect } from './middleware/nip19-redirect.ts';
import { nodeInfoController, nodeInfoSchemaController } from './nodeinfo.ts';
import { nostrController } from './nostr/nip05.ts';
import { hostMetaController, webfingerController } from './webfinger/controller.ts';

const app = new Hono();

app.get('*', cache({ cacheName: 'web', expires: 3600 }));
app.use('*', cors());

app.get('/.well-known/webfinger', webfingerController);
app.get('/.well-known/host-meta', hostMetaController);
app.get('/.well-known/nodeinfo', nodeInfoController);
app.get('/.well-known/nostr.json', nostrController);

app.get('/users/:pubkey/followers', followersController);
app.get('/users/:pubkey/following', followingController);
app.get('/users/:pubkey/outbox', outboxController);
app.use('/users/:pubkey', nip19Redirect());
app.get('/users/:pubkey', actorController);
app.use('/objects/:id', nip19Redirect());
app.get('/objects/:id', objectController);

app.get('/nodeinfo/:version', nodeInfoSchemaController);
app.post('/users/:username/inbox', inboxController);
app.post('/inbox', inboxController);

app.get('/tags/:tag', (c) => c.json([]));

app.get('/', indexController);

app.get('*', () => {
  return new Response('', { status: 404 });
});

export default app;
