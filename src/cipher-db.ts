import { Sqlite } from './deps.ts';

interface CipherIds {
  apId: string;
  nostrId: string;
}

class CipherDB {
  #db: Sqlite;

  constructor(db: Sqlite) {
    this.#db = db;

    this.#db.execute(`
      CREATE TABLE IF NOT EXISTS cipher (
        ap_id VARCHAR UNIQUE,
        nostr_id VARCHAR UNIQUE
      );
    `);
  }

  add(ids: CipherIds): void {
    this.#db.query('INSERT OR REPLACE INTO cipher (ap_id, nostr_id) VALUES (?, ?)', [ids.apId, ids.nostrId]);
  }

  getApId(nostrId: string) {
    const result = this.#db.query<string[]>('SELECT ap_id FROM cipher WHERE nostr_id = ?', [nostrId]);
    return result[0] ? result[0][0] : undefined;
  }

  getNostrId(apId: string) {
    const result = this.#db.query<string[]>('SELECT nostr_id FROM cipher WHERE ap_id = ?', [apId]);
    return result[0] ? result[0][0] : undefined;
  }
}

export default CipherDB;
