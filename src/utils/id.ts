import { LOCAL_DOMAIN } from '../config.ts';

/** Generate a random ActivityPub ID. */
const generateId = (prefix: string): string => {
  return new URL(`/${prefix}/${crypto.randomUUID()}`, LOCAL_DOMAIN).toString();
};

export { generateId };
