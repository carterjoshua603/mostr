import { LOCAL_DOMAIN } from '../config.ts';

const url = (path: string) => new URL(path, LOCAL_DOMAIN).toString();
const date = (timestamp: number) => (new Date(timestamp * 1000)).toISOString();

const pubkeyToApId = (pubkey: string): string => url(`/users/${pubkey}`);

const nostrDate = () => Math.floor(new Date().getTime() / 1000);
const toNostrDate = (iso8601: string) => Math.floor(new Date(iso8601).getTime() / 1000);

const addHtmlBreaks = (html: string) =>
  html
    .replaceAll('><p>', '>\n\n<p>')
    .replaceAll('><pre><code>', '>\n\n<pre><code>')
    .replaceAll('><ul>', '>\n<ul>')
    .replaceAll('><ol>', '>\n<ol>')
    .replaceAll('><li>', '>\n<li>')
    .replaceAll(/<br ?\/?>/g, '\n');

const apIdToPubkey = (apId: string): string => {
  const baseUrl = url('/users/');
  if (!apId.startsWith(baseUrl)) throw apId;

  const { pathname } = new URL(apId);
  return pathname.split('/')[2];
};

function isURL(value: unknown): value is string {
  if (typeof value !== 'string') return false;
  try {
    new URL(value);
    return true;
  } catch (_e) {
    return false;
  }
}

export { addHtmlBreaks, apIdToPubkey, date, isURL, nostrDate, pubkeyToApId, toNostrDate, url };
