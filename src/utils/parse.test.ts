import { assertEquals, assertThrows } from '../deps.ts';

import { apIdToPubkey } from './parse.ts';

Deno.test('apIdToPubkey', () => {
  const pubkey = '82341f882b6eabcd2ba7f1ef90aad961cf074af15b9ef44a09f9d2a8fbfbe6a2';
  const apId = `http://localhost:8000/users/${pubkey}`;
  assertEquals(apIdToPubkey(apId), pubkey);

  assertThrows(() => {
    apIdToPubkey(`http://example.com/users/${pubkey}`);
  });
});
