import { Sqlite } from './deps.ts';

class FollowsDB {
  #db: Sqlite;

  constructor(db: Sqlite) {
    this.#db = db;

    this.#db.execute(`
      CREATE TABLE IF NOT EXISTS follows (
        follower_id VARCHAR,
        followed_id VARCHAR
      );

      CREATE INDEX IF NOT EXISTS idx_follows_follower_id ON follows (follower_id);
      CREATE INDEX IF NOT EXISTS idx_follows_followed_id ON follows (followed_id);
      CREATE UNIQUE INDEX IF NOT EXISTS idx_follows_unique ON FOLLOWS (follower_id, followed_id);
    `);
  }

  addFollow(followerId: string, followedId: string): void {
    this.#db.query('INSERT OR IGNORE INTO follows (follower_id, followed_id) VALUES (?, ?)', [followerId, followedId]);
  }

  removeFollow(followerId: string, followedId: string): void {
    this.#db.query('DELETE FROM follows WHERE follower_id = ? AND followed_id = ?', [followerId, followedId]);
  }

  getFollowers(followedId: string): string[] {
    return this.#db.query<string[]>('SELECT follower_id FROM follows WHERE followed_id = ?', [followedId])
      .map((row) => row[0]);
  }

  hasFollowers(followedId: string): boolean {
    const result = this.#db.query<number[]>('SELECT COUNT(*) FROM follows WHERE followed_id = ?', [followedId]);
    return result[0][0] > 0;
  }

  getFollows(followerId: string): string[] {
    return this.#db.query<string[]>('SELECT followed_id FROM follows WHERE follower_id = ?', [followerId])
      .map((row) => row[0]);
  }
}

export default FollowsDB;
